from cmd_manager import *
from datetime import datetime

def private_channel_request(self, manager, opts, arg, channel, sender):
	if self.channels.is_valid(arg):
		self.msg(channel, "I'm already in @b%s@b." % arg)
	elif self.channels.is_banned(arg):
		chan = self.channels[arg]
		self.elog.request('Request for banned channel @b%s@b from @b%s@b.' % (arg, sender))
		message = 'Request failed, channel @b%s@b was banned by @b%s@b.' % (arg, chan.ban_source)

		if chan.ban_reason != None:
			message += ' Reason: @b%s@b.' % chan.ban_reason

		if chan.ban_expiry != None:
			message += ' Expires: @b%s@b.' % datetime.fromtimestamp(chan.ban_expiry)

		self.msg(channel, message)
		self.msg(channel, 'Please email @c3erepublik@rizon.net@o to appeal.')
	else:
		self.auth.request(sender, arg, 'request')

def private_channel_remove(self, manager, opts, arg, channel, sender):
	if not self.channels.is_valid(arg):
		self.msg(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'remove')

def private_channel_news(self, manager, opts, arg, channel, sender):
	if not self.channels.is_valid(arg):
		self.msg(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'news')

def private_channel_nonews(self, manager, opts, arg, channel, sender):
	if not self.channels.is_valid(arg):
		self.msg(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'nonews')

def private_channel_mass(self, manager, opts, arg, channel, sender):
	if not self.channels.is_valid(arg):
		self.msg(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'chanmass')

def private_channel_nomass(self, manager, opts, arg, channel, sender):
	if not self.channels.is_valid(arg):
		self.msg(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'nochanmass')

def private_user_mass(self, manager, opts, arg, channel, sender):
	self.auth.request(sender, sender, 'usermass')

def private_user_nomass(self, manager, opts, arg, channel, sender):
	self.auth.request(sender, sender, 'nousermass')

def private_help(self, manager, opts, arg, channel, sender):
	command = arg.lower()

	if command == '':
		message = manager.get_help()
	else:
		message = manager.get_help(command)

		if message == None:
			message = ['%s is not a valid command.' % arg]

	for line in message:
		self.msg(channel, line)

class PrivateCommandManager(CommandManager):
	def get_prefix(self):
		return ''

	def get_invalid(self):
		return 'Invalid message. Say help for a list of valid messages.'

	def get_commands(self):
		return {
			'request': (private_channel_request, ARG_YES|ARG_OFFLINE, 'requests a channel (must be founder)', [], '#channel'),
			'remove': (private_channel_remove, ARG_YES|ARG_OFFLINE, 'removes a channel (must be founder)', [], '#channel'),
			'news': (private_channel_news, ARG_YES|ARG_OFFLINE, 'enables news broadcasting on the channel (must be founder)', [], '#channel'),
			'nonews': (private_channel_nonews, ARG_YES|ARG_OFFLINE, 'disables news broadcasting on the channel (must be founder)', [], '#channel'),
			'chanmass': (private_channel_mass, ARG_YES|ARG_OFFLINE, 'enables mass highlighting on the channel (must be founder)', [], '#channel'),
			'nochanmass': (private_channel_nomass, ARG_YES|ARG_OFFLINE, 'disables mass highlighting on the channel (must be founder)', [], '#channel'),
			'usermass': (private_user_mass, ARG_NO|ARG_OFFLINE, 'includes your nick in mass highlights', []),
			'nousermass': (private_user_nomass, ARG_NO|ARG_OFFLINE, 'exclude your nick from mass highlights', []),
			'hi': 'help',
			'hello': 'help',
			'help': (private_help, ARG_OPT, 'displays help text', []),
		}

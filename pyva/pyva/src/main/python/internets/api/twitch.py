import json
from urllib import urlopen

class Twitch(object):
	api_url = 'https://api.twitch.tv/kraken'

	def search(self, suburl):
		response = urlopen(self.api_url + suburl)
		return json.load(response)

	def get_streams(self):
		streams = []
		data = self.search('/streams')
		for s in data['streams']:
			streams.append({'game': s['game'], 'viewers': s['viewers'], 'created_at':
					unicode(s['created_at']).replace('T', ' ').replace('Z', ''),
					'channel': s['_links']['self'][(s['_links']['self'].rfind('/')+1):]})
		return streams

	def get_channel(self, cname):
		videos = []
		data = self.search('/channels/' + cname + '/videos')
		if 'videos' in data:
			for v in data['videos']:
				videos.append({'title': v['title'], 'url': v['url'], 'views': v['views']})
			return videos

	def search_stream(self, query):
		streams = []
		data = self.search('/search/streams?query=' + query)
		total = data['_total']
		for s in data['streams']:
			streams.append({'game': s['game'], 'views': s['channel']['views'], 'followers': s['channel']['followers'],
					'url': s['channel']['url'], 'chan': s['channel']['name'], 'status': s['channel']['status']})
		return [total, streams]

	def search_games(self, query):
		games = []
		data = self.search('/search/games?query=' + query + '&type=suggest')
		for g in data['games']:
			games.append({'name': g['name'], 'popularity': g['popularity'], 'name2': g['name'].replace(' ', '+')})
		return games

	def get_teams(self):
		teams = []
		data = self.search('/teams')
		for t in data['teams']:
			teams.append({'name': t['name'], 'display_name': t['display_name'], 'info': t['info'], 'created_at':
					unicode(t['created_at']).replace('T', ' ').replace('Z', '')})
		return teams

	def get_team(self, tname):
		data = self.search('/teams/' + tname)
		if 'name' in data:
			team = {'name': data['name'], 'display_name': data['display_name'], 'info': data['info'],
				'created_at': unicode(data['created_at']).replace('T', ' ').replace('Z', '')}
			return team

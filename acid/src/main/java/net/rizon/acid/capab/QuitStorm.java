/*
 * Copyright (c) 2016, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.capab;

import com.google.common.eventbus.EventBus;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import net.rizon.acid.events.EventQuit;
import net.rizon.acid.events.EventServerDelink;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class QuitStorm
{
	private Collection<User> users;
	private EventBus eventBus;

	public QuitStorm(
			EventBus eventBus,
			Collection<User> users)
	{
		this.eventBus = eventBus;
		this.users = users;
	}

	public void onServerDelink(Server server)
	{
		// Create clone to prevent concurrent modification exception.
		Collection<Server> servers = new HashSet<>(server.getChildren());

		for (Server s : servers)
		{
			this.onServerDelink(s);
		}

		Collection<User> serverUsers = getUsersOnServer(server);

		for (User user : serverUsers)
		{
			EventQuit quitEvent = new EventQuit();

			quitEvent.setUser(user);
			quitEvent.setMsg("*.net *.split");
			quitEvent.setQuitStorm(true);

			eventBus.post(quitEvent);

			user.onQuit();
		}

		EventServerDelink delinkEvent = new EventServerDelink();
		delinkEvent.setServer(server);
		eventBus.post(delinkEvent);

		server.onQuit();
	}

	private Collection<User> getUsersOnServer(Server server)
	{
		return users.stream()
				.filter(u -> u.getServer() == server)
				.collect(Collectors.toSet());
	}
}

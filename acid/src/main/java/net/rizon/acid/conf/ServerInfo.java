package net.rizon.acid.conf;

import net.rizon.acid.util.CloakGenerator;

public class ServerInfo implements Validatable
{
	public String name, description, id;
	public String network;
	public String[] cloakkeys;

	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("name", name);
		Validator.validateNotEmpty("description", description);
		Validator.validateNotEmpty("id", id);
		Validator.validateNotEmpty("network", network);
		Validator.validateNotNull("cloakkeys", cloakkeys);
		if (cloakkeys.length != 3)
		{
			throw new ConfigException("Cloak system must have 3 keys.");
		}
		for (int i = 0; i < 3; i++)
		{
			if (!CloakGenerator.validateCloakKey(cloakkeys[i]))
			{
				throw new ConfigException("Cloak key " + (i + 1) + " is not random enough.");
			}
		}
	}
}
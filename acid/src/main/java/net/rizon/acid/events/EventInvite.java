package net.rizon.acid.events;

import net.rizon.acid.core.Channel;
import net.rizon.acid.core.User;

public class EventInvite
{
	private User inviter;
	private User invitee;
	private Channel channel;

	public User getInviter()
	{
		return inviter;
	}

	public void setInviter(User inviter)
	{
		this.inviter = inviter;
	}

	public User getInvitee()
	{
		return invitee;
	}

	public void setInvitee(User invitee)
	{
		this.invitee = invitee;
	}

	public Channel getChannel()
	{
		return channel;
	}

	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}
}

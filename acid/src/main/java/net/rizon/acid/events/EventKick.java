package net.rizon.acid.events;

import net.rizon.acid.core.Channel;
import net.rizon.acid.core.User;

public class EventKick
{
	private String kicker;
	private User victim;
	private Channel channel;
	private String reason;

	public String getKicker()
	{
		return kicker;
	}

	public void setKicker(String kicker)
	{
		this.kicker = kicker;
	}

	public User getVictim()
	{
		return victim;
	}

	public void setVictim(User victim)
	{
		this.victim = victim;
	}

	public Channel getChannel()
	{
		return channel;
	}

	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}
}

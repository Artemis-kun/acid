package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Kick extends Message
{
	private static final Logger log = LoggerFactory.getLogger(Kick.class);

	public Kick()
	{
		super("KICK");
	}

	// :99hAAAAAB KICK #geo 99hAAAAAB :Lame and fake
	private void run(String source, String[] params)
	{
		String channel = params[0];
		User kickee = User.findUser(params[1]);
		if (kickee == null)
		{
			log.warn("KICK for nonexistent user " + params[1]);
			return;
		}

		Channel chan = Channel.findChannel(channel);
		if (chan == null)
		{
			log.warn("KICK from " + source + " for " + kickee.getNick() + " on nonexistent channel " + channel);
			return;
		}

		chan.removeUser(kickee);
		kickee.remChan(chan);

		Acidictive.onKick(source, kickee, chan, params[2]);
	}

	@Override
	public void onUser(User source, String[] params)
	{
		run(source.getNick(), params);
	}

	@Override
	public void onServer(Server source, String[] params)
	{
		run(source.getName(), params);
	}
}

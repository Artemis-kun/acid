package net.rizon.acid.messages;

import net.rizon.acid.core.Message;
import net.rizon.acid.core.Protocol;

public class Ping extends Message
{
	public Ping()
	{
		super("PING");
	}

	@Override
	public void on(String source, String[] params)
	{
		Protocol.pong(params[0]);
	}
}
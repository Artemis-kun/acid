package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQuit extends Message
{
	private static final Logger log = LoggerFactory.getLogger(SQuit.class);

	public SQuit()
	{
		super("SQUIT");
	}

	// SQUIT test.geodns.org :irc.geodns.org
	// SQUIT 98C :jupe over

	@Override
	public void on(String source, String[] params)
	{
		Server x = Server.findServer(params[0]);
		if (x == null)
		{
			log.warn("SQUIT for nonexistent server " + params[0]);
			return;
		}

		Acidictive.onSquit(x);
	}
}
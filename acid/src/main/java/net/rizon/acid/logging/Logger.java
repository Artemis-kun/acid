package net.rizon.acid.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.StackTraceElementProxy;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import java.util.ArrayList;
import java.util.List;
import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.User;
import net.rizon.acid.util.Format;

public class Logger extends UnsynchronizedAppenderBase<ILoggingEvent>
{

	private String calculateColor(Level level)
	{
		switch (level.toInt())
		{
			case Level.DEBUG_INT:
				return Format.color(Format.GREY);
			case Level.INFO_INT:
				return "";
			case Level.WARN_INT:
				return Format.color(Format.ORANGE);
			case Level.ERROR_INT:
				return Format.color(Format.RED);
			default:
				return "";
		}
	}
	
	@Override
	protected void append(ILoggingEvent event)
	{
		Level level = event.getLevel();
		if (!level.isGreaterOrEqual(Level.INFO))
		{
			return;
		}

		String message = event.getFormattedMessage();
		IThrowableProxy throwable = event.getThrowableProxy();
		StackTraceElement[] stes = null;

		if (throwable != null)
		{
			List<StackTraceElement> list = new ArrayList<>();
			for (StackTraceElementProxy step : throwable.getStackTraceElementProxyArray())
			{
				list.add(step.getStackTraceElement());
			}
			stes = list.toArray(new StackTraceElement[0]);
		}

		if (AcidCore.me != null && !AcidCore.me.isBursting() && User.findUser(Acidictive.conf.general.control) != null)
		{
			String routingSpam = Acidictive.conf.getChannelNamed("routing-spam");

			if (message != null)
			{
				Acidictive.privmsg(routingSpam, calculateColor(event.getLevel()) + Format.BOLD + event.getLevel() + Format.BOLD + Format.color(0) + " (" + event.getCallerData()[0].getFileName() + "): " + message);
			}

			if (throwable != null)
			{
				String exception = throwable.getMessage() == null ? throwable.getClassName() : throwable.getMessage();
				
				if (exception != null)
				{
					Acidictive.privmsg(routingSpam, exception);
				}
			}
			if (stes != null)
			{
				for (StackTraceElement ste : stes)
				{
					Acidictive.privmsg(routingSpam, ste.toString());
				}
			}
		}
	}
}

package net.rizon.acid.plugins;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.Manifest;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.User;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.resolution.ArtifactResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PluginManager
{
	private static final Logger logger = LoggerFactory.getLogger(PluginManager.class);

	private static List<Plugin> plugins = new LinkedList<>();

	private static Manifest getManifest(File jar) throws IOException
	{
		String manifestPath = "jar:file:" + jar.getAbsolutePath() + "!/META-INF/MANIFEST.MF";

		URL url = new URL(manifestPath);

		URLConnection con = url.openConnection();
		con.setUseCaches(false);

		try (InputStream manifestInputStream = con.getInputStream())
		{
			return new Manifest(manifestInputStream);
		}
	}

	public static Plugin loadPlugin(String groupId, String artifactId, String version) throws Exception
	{
		Artifact a = new DefaultArtifact(groupId, artifactId, "", "jar", version);

		Plugin p = findPlugin(a);
		if (p != null)
		{
			return p;
		}

		ArtifactResolver resolver = new ArtifactResolver();
		List<ArtifactResult> artifacts = resolver.resolveArtifacts(a);

		ArtifactResult artifact = artifacts.remove(0);
		// artifacts contains dependencies now

		logger.debug("Located artifact {}: {}", artifact, artifact.getArtifact().getFile());

		Manifest mf = getManifest(artifact.getArtifact().getFile());

		String mainClass = mf.getMainAttributes().getValue("Main-Class");
		String exportBase = mf.getMainAttributes().getValue("Export-Base");
		if (mainClass == null || exportBase == null)
		{
			logger.warn("No main class or export base for plugin {}", a);
			return null;
		}

		ClassLoader cl = new ClassLoader(artifact.getArtifact().getFile(), exportBase);
		try
		{

			for (ArtifactResult a2 : artifacts)
			// XXX I think if this is another plugin it won't work right?
			{
				cl.addFile(a2.getArtifact().getFile());
			}

			Class<?> c;
			try
			{
				c = cl.loadClass(mainClass);
			}
			catch (ClassNotFoundException ex)
			{
				logger.warn("unable to load main class", ex);
				return null;
			}

			Constructor<?> con = c.getConstructor();

			p = (Plugin) con.newInstance();

			p.artifact = artifact.getArtifact();
			p.loader = cl;
			cl = null;
			p.manifest = mf;
			p.setName(artifactId);

			p.start();

			plugins.add(p);

			return p;
		}
		finally
		{
			if (cl != null)
			{
				cl.close();
			}
		}
	}

	public static void unload(Plugin plugin)
	{
		// Detach plugin from clients
		for (User u : User.getUsersC())
		{
			if (u instanceof AcidUser)
			{
				AcidUser au = (AcidUser) u;

				if (au.pkg == plugin)
					au.pkg = null;
			}
		}

		try
		{
			plugin.stop();
		}
		catch (Exception ex)
		{
			logger.warn("Error stopping plugin!", ex);
		}

		plugins.remove(plugin);
	}

	public static final Plugin[] getPlugins()
	{
		Plugin[] a = new Plugin[plugins.size()];
		plugins.toArray(a);
		return a;
	}

	public static Plugin findPlugin(String name)
	{
		for (Plugin p : plugins)
		{
			if (p.getName().equals(name))
			{
				return p;
			}
		}
		return null;
	}

	public static Plugin findPlugin(Artifact artifact)
	{
		for (Plugin p : plugins)
		{
			if (p.artifact.equals(artifact))
			{
				return p;
			}
		}
		return null;
	}
}

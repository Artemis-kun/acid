/*
 * Copyright (c) 2016, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.capab;

import com.google.common.eventbus.EventBus;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class QuitStormTest
{
	private EventBus testEventBus;
	private Collection<User> testUsers;
	private QuitStorm quitStorm;

	private Server mockAcid;
	private Server mockLink;
	private User mockLinkUser;
	private User mockAcidUser;

	@BeforeClass
	public static void setUpClass()
	{
	}

	@AfterClass
	public static void tearDownClass()
	{
	}

	@Before
	public void setUp()
	{
		testUsers = new HashSet<>();
		testEventBus = new EventBus();

		quitStorm = new QuitStorm(testEventBus, testUsers);

		mockAcid = mock(Server.class);
		mockLink = mock(Server.class);

		mockLinkUser = mock(User.class);
		mockAcidUser = mock(User.class);

		when(mockLink.getHub()).thenReturn(mockAcid);
		when(mockAcid.getHub()).thenReturn(null);

		when(mockLinkUser.getServer()).thenReturn(mockLink);
		when(mockAcidUser.getServer()).thenReturn(mockAcid);

		Set<Server> linkedServers = new HashSet<>();
		linkedServers.add(mockLink);
		testUsers.add(mockLinkUser);
		testUsers.add(mockAcidUser);

		when(mockAcid.getChildren()).thenReturn(linkedServers);
		when(mockLink.getChildren()).thenReturn(new HashSet<>());

		testEventBus.register(quitStorm);
	}

	@After
	public void tearDown()
	{
	}

	/**
	 * Server Graph: Acid - Link
	 * <p>
	 * Test Link disconnecting from Acid and have all users of Link quitting due
	 * to quit storm.
	 */
	@Test
	public void testOnServerDelink()
	{
		quitStorm.onServerDelink(mockLink);

		// Check if quit is called the correct amount of times.
		verify(mockLink, times(1)).onQuit();
		verify(mockLinkUser, times(1)).onQuit();

		verify(mockAcid, never()).onQuit();
		verify(mockAcidUser, never()).onQuit();
	}

	/**
	 * Server Graph: Acid - Link - C
	 * <p>
	 * Test Link and C disconnecting from Acid and have all users on Link and C
	 * quitting	and have C SQuitting.
	 */
	@Test
	public void testOnServerDelinkTwoServers()
	{
		Server mockC = mock(Server.class);
		User userC = mock(User.class);

		when(mockC.getHub()).thenReturn(mockLink);
		when(mockC.getChildren()).thenReturn(new HashSet<>());
		when(userC.getServer()).thenReturn(mockC);

		mockLink.getChildren().add(mockC);
		testUsers.add(userC);

		quitStorm.onServerDelink(mockLink);

		// Check if quit is called the correct amount of times.
		verify(mockC, times(1)).onQuit();
		verify(userC, times(1)).onQuit();

		verify(mockLink, times(1)).onQuit();
		verify(mockLinkUser, times(1)).onQuit();

		verify(mockAcid, never()).onQuit();
		verify(mockAcidUser, never()).onQuit();
	}

	/**
	 * Server Graph:
	 * <p>
	 * <code>
	 * C - Link - Acid
	 * D /
	 * </code>
	 */
	@Test
	public void testOnServerDelinkThreeServersT()
	{
		Server mockC = mock(Server.class);
		Server mockD = mock(Server.class);

		User userC = mock(User.class);
		User userD = mock(User.class);

		when(mockC.getHub()).thenReturn(mockLink);
		when(mockC.getChildren()).thenReturn(new HashSet<>());
		when(mockD.getHub()).thenReturn(mockLink);
		when(mockD.getChildren()).thenReturn(new HashSet<>());
		when(userC.getServer()).thenReturn(mockC);
		when(userD.getServer()).thenReturn(mockD);

		mockLink.getChildren().add(mockC);
		mockLink.getChildren().add(mockD);
		testUsers.add(userC);
		testUsers.add(userD);

		quitStorm.onServerDelink(mockLink);

		// Check if quit is called the correct amount of times.
		verify(mockC, times(1)).onQuit();
		verify(mockD, times(1)).onQuit();
		verify(userC, times(1)).onQuit();
		verify(userD, times(1)).onQuit();

		verify(mockLink, times(1)).onQuit();
		verify(mockLinkUser, times(1)).onQuit();

		verify(mockAcid, never()).onQuit();
		verify(mockAcidUser, never()).onQuit();
	}

	/**
	 * Server Graph: Acid - Link
	 * <p>
	 * Have multiple users on Link and verify them all disconnecting.
	 */
	@Test
	public void testOnServerDelinkMultipleUsers()
	{
		User userC = mock(User.class);
		User userD = mock(User.class);

		when(userC.getServer()).thenReturn(mockLink);
		when(userD.getServer()).thenReturn(mockLink);

		testUsers.add(userC);
		testUsers.add(userD);

		quitStorm.onServerDelink(mockLink);

		// Check if quit is called the correct amount of times.
		verify(userC, times(1)).onQuit();
		verify(userD, times(1)).onQuit();

		verify(mockLink, times(1)).onQuit();
		verify(mockLinkUser, times(1)).onQuit();

		verify(mockAcid, never()).onQuit();
		verify(mockAcidUser, never()).onQuit();
	}

	/**
	 * Server Graph: C - Acid - Link
	 * <p>
	 * Have multiple users on Link and verify them all disconnecting.
	 */
	@Test
	public void testOnServerDelinkServersAlive()
	{
		Server mockC = mock(Server.class);
		User userC = mock(User.class);

		when(mockC.getHub()).thenReturn(mockAcid);
		when(mockC.getChildren()).thenReturn(new HashSet<>());
		when(userC.getServer()).thenReturn(mockC);

		testUsers.add(userC);

		quitStorm.onServerDelink(mockLink);

		// Check if quit is called the correct amount of times.
		verify(mockC, never()).onQuit();
		verify(userC, never()).onQuit();

		verify(mockLink, times(1)).onQuit();
		verify(mockLinkUser, times(1)).onQuit();

		verify(mockAcid, never()).onQuit();
		verify(mockAcidUser, never()).onQuit();
	}
}

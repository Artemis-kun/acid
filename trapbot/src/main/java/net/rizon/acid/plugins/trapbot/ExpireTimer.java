package net.rizon.acid.plugins.trapbot;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

class ExpireTimer implements Runnable
{
	@Override
	public void run()
	{
		Date now = new Date();
		
		for (Iterator<Map.Entry<String, TrappedUser>> it = trapbot.users.entrySet().iterator(); it.hasNext();)
		{
			Map.Entry<String, TrappedUser> e = it.next();
			TrappedUser t = e.getValue();

			/* how many minutes have passed */
			int minutes = (int)((now.getTime() - t.lastTrap.getTime()) / (1000 * 60));

			if (minutes >= trapbot.conf.expire)
				it.remove();
		}
	}
}
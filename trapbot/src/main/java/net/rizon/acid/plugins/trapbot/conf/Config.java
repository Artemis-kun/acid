package net.rizon.acid.plugins.trapbot.conf;

import java.util.List;
import net.rizon.acid.conf.Client;
import net.rizon.acid.conf.ConfigException;
import net.rizon.acid.conf.Configuration;

public class Config extends Configuration
{
	public List<Client> clients;

	public String trapbot, trapchan;
	public int expire;

	@Override
	public void validate() throws ConfigException
	{
	}
}

package net.rizon.acid.plugins.xmas.conf;

public class VhostDay
{
	private String date;
	private String vhost;

	public VhostDay() {}

	public VhostDay(String date, String vhost)
	{
		this.date = date;
		this.vhost = vhost;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public String getVhost()
	{
		return vhost;
	}

	public void setVhost(String vhost)
	{
		this.vhost = vhost;
	}
}

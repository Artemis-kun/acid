package net.rizon.acid.plugins.xmas.events;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Membership;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.xmas.Xmas;

public class Voice implements Runnable
{
	private static final Random rando = new Random();

	@Override
	public void run()
	{
		// We're not even being festive :(
		if (Xmas.todaysVhost() == null)
			return;

		final Channel channel = Channel.findChannel(Xmas.getXmasChanName());
		if (channel == null)
			return;

		final Collection<Membership> users = channel.getMembers();
		final List<Membership> unvoiced = users.stream()
			.filter(m -> !m.hasVoice())
			.collect(Collectors.toList());

		// Everyone is special
		if (unvoiced.size() < 1)
			return;

		final User u = unvoiced.get(rando.nextInt(unvoiced.size())).user;

		// Internets and netsec with coloured vhosts are cute, but let's not
		if (u.getServer() == AcidCore.me || u.getServer().isUlined())
			return;

		if (User.findUser("BotServ") != null)
			Acidictive.privmsg("BotServ", String.format("SAY %s Congratulations, %s! You have been drawn for the special vhost.", channel.getName(), u.getNick()));
		if (User.findUser("ChanServ") != null)
			Acidictive.privmsg("ChanServ", String.format("VOICE %s %s", channel.getName(), u.getNick()));
	}
}

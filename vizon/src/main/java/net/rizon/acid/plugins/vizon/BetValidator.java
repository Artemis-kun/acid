/*
 * Copyright (c) 2016, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Static class which validates a bet placed by a user.
 *
 * @author orillion <orillion@rizon.net>
 */
public class BetValidator
{
	private static final Pattern BET_PATTERN
		= Pattern.compile("^\\s*(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s*$");

	public static final int BET_MIN = 1;
	public static final int BET_MAX = 29; // inclusive

	/**
	 * Attempts to validate if a bet placed by a user is within [1,29]. Bets
	 * are done via <code>!bet first second third fourth fifth sixth</code>.
	 * This function expects to get the string without !bet part and then
	 * parses it into an {@link Optional}.
	 *
	 * @param bet Bet the user placed.
	 *
	 * @return {@link Optional} which contains the bet, or is empty if not
	 * parsable.
	 */
	public static Optional<Bet> validate(String bet)
	{
		if (bet == null)
		{
			return Optional.empty();
		}

		Matcher matcher = BET_PATTERN.matcher(bet);

		if (!matcher.find())
		{
			return Optional.empty();
		}

		List<Integer> numbers = new ArrayList<>();

		for (int i = 1; i <= matcher.groupCount(); i++)
		{
			// Guaranteed to be integers due to regex.
			int number = Integer.parseInt(matcher.group(i));

			if (!validNumber(number))
			{
				// Number is not between 1 and 29 inclusive
				return Optional.empty();
			}

			if (numbers.contains(number))
			{
				// Number already occured before
				return Optional.empty();
			}

			numbers.add(number);
		}

		Bet betValue = new Bet(
			numbers.get(0),
			numbers.get(1),
			numbers.get(2),
			numbers.get(3),
			numbers.get(4),
			numbers.get(5));

		return Optional.of(betValue);
	}

	private static boolean validNumber(int number)
	{
		return number >= BET_MIN && number <= BET_MAX;
	}
}

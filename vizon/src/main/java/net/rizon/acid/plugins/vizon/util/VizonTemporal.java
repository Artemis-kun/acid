/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.util;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import net.rizon.acid.plugins.vizon.conf.VizonConfig;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VizonTemporal
{
	/**
	 * Determines the next drawing that should place, after the specified date.
	 *
	 * @param date   Date after which the next drawing should be determined.
	 * @param config Config file
	 *
	 * @return Next date
	 */
	public static LocalDateTime determineNextDrawing(LocalDateTime date, VizonConfig config)
	{
		LocalTime drawingTime = LocalTime.parse(config.drawingTime);
		LocalDateTime drawing = LocalDateTime.MAX;

		/*
		 * This for loop constructs a LocalDateTime object for every day that is
		 * in the config and then checks if it is closer to now than the
		 * previous one it determined.
		 */
		for (int dayOfWeek : config.days)
		{
			LocalDateTime temp = date
					.with(TemporalAdjusters.nextOrSame(DayOfWeek.of(dayOfWeek)))
					.withHour(drawingTime.getHour())
					.withMinute(drawingTime.getMinute())
					.withSecond(drawingTime.getSecond());

			if (temp.isAfter(date) && temp.isBefore(drawing))
			{
				drawing = temp;
			}
		}

		return drawing;
	}
}

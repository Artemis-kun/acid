/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.conf;

import java.time.Duration;
import java.util.List;
import net.rizon.acid.arguments.ExpiryArgument;
import net.rizon.acid.conf.Client;
import net.rizon.acid.conf.ConfigException;
import net.rizon.acid.conf.Configuration;
import net.rizon.acid.conf.Validator;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VizonConfig extends Configuration
{

	public List<Client> clients;
	public String vizonChannel;
	public String vizonDebugChannel;
	public String vizonBot;
	public List<Integer> days;
	public String drawingOpen;
	public String drawingClose;
	public String drawingTime;

	public int firstPrize;
	public int secondPrize;
	public int thirdPrize;
	public int consolationPrize;

	private ExpiryArgument drawingOpenArgument;
	private ExpiryArgument drawingCloseArgument;

	@Override
	public void validate() throws ConfigException
	{
		drawingOpenArgument = ExpiryArgument.parse(drawingOpen);
		drawingCloseArgument = ExpiryArgument.parse(drawingClose);

		Validator.validateNotNull("drawingOpenArgument", drawingOpenArgument);
		Validator.validateNotNull("drawingCloseArgument", drawingCloseArgument);
		Validator.validateNotNull("days", days);
	}

	public Duration getOpenTimeInterval()
	{
		return drawingOpenArgument.getDuration();
	}

	public Duration getCloseTimeInterval()
	{
		return drawingCloseArgument.getDuration();
	}
}

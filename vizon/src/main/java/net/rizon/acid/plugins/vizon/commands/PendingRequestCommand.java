/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.commands;

import java.util.List;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.vizon.RequestTracker;
import net.rizon.acid.plugins.vizon.Vizon;
import net.rizon.acid.plugins.vizon.db.VizonRequest;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class PendingRequestCommand extends Command
{
	public PendingRequestCommand()
	{
		super(0, 0);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		List<VizonRequest> requests = Vizon.getRequestTracker().getPending();

		if (requests.isEmpty())
		{
			Acidictive.reply(source, to, c, "No pending colored vhosts");
			return;
		}

		for (int index = 0; index < requests.size(); index++)
		{
			VizonRequest request = requests.get(index);

			if (request == null)
			{
				continue;
			}

			Acidictive.reply(source, to, c, String.format(
					RequestTracker.MESSAGE_FORMAT,
					index + 1,
					request.getNick(),
					request.getVhost()));
		}
	}
}
